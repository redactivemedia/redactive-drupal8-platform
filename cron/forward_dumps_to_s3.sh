#!/usr/bin/env bash

# See the forward_dumps_to_s3_README.md file in this directory for destructions.

# We set the filename to the current date and time; this leaves us with three
# backups per day.
#DATE=$(date +%Y-%m-%d)
DATETIME=$(date '+%Y%m%d-%H%M')

# Get the S3 directory name without its trailing slash - we prepend this to the
# dump name.
AWS_S3_FOLDER_NO_SLASH="${AWS_S3_FOLDER::-1}"

DUMP_FILE_NAME="backup--${AWS_S3_FOLDER_NO_SLASH}--${PLATFORM_ENVIRONMENT}--${DATETIME}--main-database.sql"
LATEST_DUMP_FILE_NAME="_latest-backup--${PLATFORM_ENVIRONMENT}.txt"

# If we have all the variables set, take the backup and send it to S3.
if [[ ! -z "$AWS_S3_DB_HOST" ]] && [[ ! -z "$AWS_S3_DB_PORT" ]] && [[ ! -z "$AWS_S3_DB_USER" ]] && [[ ! -z "$AWS_S3_DB_DUMP_DIRECTORY" ]] && [[ ! -z "$AWS_S3_BUCKET" ]]; then
  echo "Backing up databases to S3 with the following settings:
- DUMP_FILE_NAME: $DUMP_FILE_NAME
- AWS_S3_DB_HOST: $AWS_S3_DB_HOST
- AWS_S3_DB_PORT: $AWS_S3_DB_PORT
- AWS_S3_DB_USER: $AWS_S3_DB_USER
- AWS_S3_DB_PASSWORD: (not shown)
- AWS_S3_DB_DUMP_DIRECTORY: $AWS_S3_DB_DUMP_DIRECTORY
- AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY: (not shown)
- AWS_S3_BUCKET: $AWS_S3_BUCKET
- AWS_S3_FOLDER: $AWS_S3_FOLDER
"

  echo "
  Checking the dump directory exists at ${AWS_S3_DB_DUMP_DIRECTORY}/${AWS_S3_FOLDER}:"
  mkdir -p "${AWS_S3_DB_DUMP_DIRECTORY}/${AWS_S3_FOLDER}" || {
    echo "Uh-oh, stopping: unable to mkdir -p ${AWS_S3_DB_DUMP_DIRECTORY}/${AWS_S3_FOLDER}"
    exit 1
  }

  echo "
  Running command: mysqldump --host=\"$AWS_S3_DB_HOST\" --port=\"$AWS_S3_DB_PORT\" --user=\"$AWS_S3_DB_USER\" --password=\"$AWS_S3_DB_PASSWORD\" --databases \"main\" > \"${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}\""

  mysqldump --host="$AWS_S3_DB_HOST" --port="$AWS_S3_DB_PORT" --user="$AWS_S3_DB_USER" --password="$AWS_S3_DB_PASSWORD" --databases "main" > "${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}"

  DUMPFAILED="no"

  # If the mysqldump command failed, change the dump file name to FAILED.txt,
  # touch that file, and don't update the latest dump name.
  if [ ! -f "${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}" ]; then
    DUMPFAILED="yes"
    DUMP_FILE_NAME="backup--${AWS_S3_FOLDER_NO_SLASH}--${PLATFORM_ENVIRONMENT}--${DATETIME}--main-database--FAILED.txt"

    touch "${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}"

    echo "This backup failed. We don't know any more about this failure, unfortunately. :-(" > "${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}"
  fi

  # If the dump worked, zip it up and upload it.
  if [ "$DUMPFAILED" == "no" ]; then
    echo "
    Running command: gzip \"${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}\":"

    # Gzip will create a file with extension .gz.
    gzip "${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}"

  #  echo "
  #  Removing dump file ${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}:"
  #  rm "${AWS_S3_DB_DUMP_DIRECTORY}/${DUMP_FILE_NAME}"

    echo "
    Updating latest file: ${LATEST_DUMP_FILE_NAME} with backup filename ${DUMP_FILE_NAME}.gz"

    echo "${DUMP_FILE_NAME}.gz" > "${AWS_S3_DB_DUMP_DIRECTORY}/${LATEST_DUMP_FILE_NAME}"

    # If we're on master, also synchronise files.
    if [[ "${PLATFORM_ENVIRONMENT}" == master* ]]; then
      echo "
      Synchronising files: aws s3 sync \"/app/web/sites/default/files/\" \"s3://${AWS_S3_BUCKET}/${AWS_S3_FOLDER}${PLATFORM_ENVIRONMENT}-files\":"

      aws s3 sync "/app/web/sites/default/files/" "s3://${AWS_S3_BUCKET}/${AWS_S3_FOLDER}files--${PLATFORM_ENVIRONMENT}"
    fi
  fi

  echo "
  Running command: aws s3 sync \"${AWS_S3_DB_DUMP_DIRECTORY}\" \"s3://${AWS_S3_BUCKET}/${AWS_S3_FOLDER}\""

  aws s3 sync "${AWS_S3_DB_DUMP_DIRECTORY}" "s3://${AWS_S3_BUCKET}/${AWS_S3_FOLDER}"

  echo "
  Emptying backup directory with rm -r ${AWS_S3_DB_DUMP_DIRECTORY}/*"

  # Note that we have to put /* outside the quotes so it can be expanded.
  rm -r "${AWS_S3_DB_DUMP_DIRECTORY}"/*

else

  echo "Unable to backup; one of the following settings variables is empty:
  - DUMP_FILE_NAME: $DUMP_FILE_NAME
  - AWS_S3_DB_HOST: $AWS_S3_DB_HOST
  - AWS_S3_DB_PORT: $AWS_S3_DB_PORT
  - AWS_S3_DB_USER: $AWS_S3_DB_USER
  - AWS_S3_DB_PASSWORD: (not shown)
  - AWS_S3_DB_DUMP_DIRECTORY: $AWS_S3_DB_DUMP_DIRECTORY
  - AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY: (not shown)
  - AWS_S3_BUCKET: $AWS_S3_BUCKET
  - AWS_S3_FOLDER: $AWS_S3_FOLDER
  "
fi
