# Forward database dumps to Amazon S3

## tl;dr

The forward_dumps_to_s3.sh shell script is used on platform.sh to dump the
databases for the current container and teleport them to an S3 bucket.

This script will also rsync the contents of the /sites/default/files directory
to the same S3 bucket, in effect creating an incremental backup of the site's
uploaded content.

This script is called by a cron job in the ./.platform.app.yaml file.

## Setup instructions

* Note that you will need the Redactive Amazon AWS login - or, you will need to adapt these instructions to send your database backups to a different S3 location
* More up-to-date instructions might be available in the [Redactive Developer Documentation](http://bit.ly/35Qt8lV)
* This work was first done in [DD-887](https://redactive.atlassian.net/browse/DD-887)

### S3: Create an S3 folder

Create a new folder for your site's database backups in the [redactive-drupal8-database-backups](https://s3.console.aws.amazon.com/s3/buckets/redactive-drupal8-database-backups?region=eu-west-2&tab=objects) folder, using the site's short name, all lowercase - e.g. `planner`.

### IAM: Create the user

#### Create the policy

1. In the [IAM Policy console](https://console.aws.amazon.com/iam/home?region=eu-west-2)
2. Copy the Json from an existing database user policy - e.g. [this one for the Actuary](https://us-east-1.console.aws.amazon.com/iam/home#/policies/arn:aws:iam::037173038346:policy/redactive-drupal8-actuary-s3-backup-user-policy$jsonEditor) and [create a new policy](https://us-east-1.console.aws.amazon.com/iam/home#/policies$new?step=edit)
3. Policy name format: `redactive-drupal8-WEBSITE-s3-backup-user-policy`
4. Change the directory name from `/actuary` to your site's name

#### Create the user

1. [Create an AWS IAM user for this site](https://us-east-1.console.aws.amazon.com/iam/home#/users$new?step=details)
2. Username format: `redactive-drupal8-WEBSITE-s3-backup-user`
3. Access type: choose `Access key - Programmatic access`
4. In "Set permissions", choose "Attach existing policies directly" and attach the new IAM policy you just created
5. **Remember to copy the key and secret into the site information document as you will need it handy when you create the platform.sh variables in the next steps**

### Set platform.sh variables

Create project-level variables using the following command (copy-paste the entire block into a text editor and change <ACCESS_KEY>, <SECRET_KEY>, and <FOLDER NAME WITH TRAILING SLASH e.g. demo/> - then paste into your terminal):

```
platform variable:create --name=AWS_ACCESS_KEY_ID --value="<ACCESS_KEY>" --level=project --json=false --sensitive=true --prefix=env --visible-build=true --visible-runtime=true; \
platform variable:create --name=AWS_SECRET_ACCESS_KEY --value="<SECRET_KEY>" --level=project --json=false --sensitive=true --prefix=env --visible-build=true --visible-runtime=true; \
platform variable:create --name=AWS_S3_BUCKET --value="redactive-drupal8-database-backups" --level=project --json=false --sensitive=false --prefix=env --visible-build=true --visible-runtime=true; \
platform variable:create --name=AWS_S3_FOLDER --value="<FOLDER NAME WITH TRAILING SLASH e.g. demo/>" --level=project --json=false --sensitive=false --prefix=env --visible-build=true --visible-runtime=true; \
platform variable:create --name=AWS_S3_DB_HOST --value="database.internal" --level=project --json=false --sensitive=false --prefix=env --visible-build=true --visible-runtime=true; \
platform variable:create --name=AWS_S3_DB_PORT --value=3306 --level=project --json=false --sensitive=false --prefix=env --visible-build=true --visible-runtime=true; \
platform variable:create --name=AWS_S3_DB_USER --value="main" --level=project --json=false --sensitive=false --prefix=env --visible-build=true --visible-runtime=true; \
platform variable:create --name=AWS_S3_DB_DUMP_DIRECTORY --value="/app/backups" --level=project --json=false --sensitive=false --prefix=env --visible-build=true --visible-runtime=true; \
platform redeploy -e master -y
```

N.b.: By default, your database password is an empty string; if it isn't in your hosting environment, you will need to set it:

```
platform variable:create --name=AWS_S3_DB_PASSWORD --value="<your database password if not empty>" --level=project --json=false --sensitive=false --prefix=env --visible-build=true --visible-runtime=true
```

### Lastly, TEST IT WORKS!

To test this script, ssh into the master environment and run it:

```
platform ssh -e master
```

... then do:

```
./cron/forward_dumps_to_s3.sh
```

Check the output for errors, then visit the [S3 backups directory](https://s3.console.aws.amazon.com/s3/buckets/redactive-drupal8-database-backups?region=eu-west-2&tab=objects) and confirm your database dump and the `_latest-backup--master-[ENVIRONMENT ID].txt`files have appeared.

### How to restore from a backup

**Before restoring a backup, it is recommended that you create a manual database
snapshot in the p.sh control panel as follows:**

1. Go to the project's p.sh control panel
2. Choose the environment you wish to restore
3. Click the "backups" tab
4. Click "Backup" in the top-right of the page

#### To roll back quickly

*Note: if you want to restore a recent database snapshot - e.g. a deployment has
borked the database irreparably, the fastest way to do
this for any platform.sh environment is:*

**<span style="color:red">WARNING: these steps WILL overwrite the database on the target environment.</span>**

1. Go to the project's p.sh control panel
2. Choose the environment you wish to restore
3. Click the "backups" tab
4. Click the "..." next to the backup you wish to restore
5. Click "Restore"

#### Restoring an older backup

The database backup archives created by this script can be downloaded from
Amazon S3 and unzipped, and then restored to a platform.sh environment with
the following command.

**<span style="color:red">WARNING: this command WILL overwrite the database on the target environment - if
you are trying to upload a database to an environment which is NOT the
production environment, PLEASE DOUBLE-CHECK THE SPECIFIED ENVIRONMENT to ensure
you're not about to overwrite the production database.</span>**

```
platform sql --relationship database -e master < unzipped-database-backup.sql
```

Note that this command can take >20 minutes for some of our multi-GB-sized databases,
and there is no progress information. You may wish to monitor the traffic being
sent from your machine - e.g. using Menumeters on a Mac. The restore process
typically maxes-out at around 800KB/s to 1MB/s.

