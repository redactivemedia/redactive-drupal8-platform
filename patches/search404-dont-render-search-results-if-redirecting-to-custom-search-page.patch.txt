diff --git src/Controller/Search404Controller.php src/Controller/Search404Controller.php
index 393cbfa..6d3599b 100644
--- src/Controller/Search404Controller.php
+++ src/Controller/Search404Controller.php
@@ -98,9 +98,45 @@ class Search404Controller extends ControllerBase {
         return $build;
       }
     }
-
-    if (\Drupal::moduleHandler()->moduleExists('search') && (\Drupal::currentUser()->hasPermission('search content') || \Drupal::currentUser()->hasPermission('search by page'))) {
-
+
+    if (\Drupal::config('search404.settings')->get('search404_do_custom_search') &&
+      !\Drupal::config('search404.settings')->get('search404_skip_auto_search')) {
+      $custom_search_path = \Drupal::config('search404.settings')->get('search404_custom_search_path');
+
+      // Remove query parameters before checking whether the search path
+      // exists or the user has access rights.
+      $custom_search_path_no_query = preg_replace('/\?.*/', '', $custom_search_path);
+      $current_path = \Drupal::service('path.current')->getPath();
+      $current_path = preg_replace('/[!@#$^&*();\'"+_,]/', '', $current_path);
+
+      // All search keywords with space
+      // and slash are replacing with hyphen in url redirect.
+      $search_keys = '';
+      // If search with OR condition enabled.
+      if (\Drupal::config('search404.settings')->get('search404_use_or')) {
+        $search_details = $this->search404CustomRedirection(' OR ', $current_path, $keys);
+      }
+      else {
+        $search_details = $this->search404CustomRedirection(' ', $current_path, $keys);
+      }
+      $current_path = $search_details['path'];
+      $search_keys = $search_details['keys'];
+
+      // Redirect to the custom path.
+      if ($current_path == "/" . $keys || $current_path == "/" . $search_keys) {
+        $this->search404CustomErrorMessage($keys);
+        if ($search_keys != '') {
+          $custom_search_path = str_replace('@keys', $search_keys, $custom_search_path);
+        }
+        return $this->search404Goto("/" . $custom_search_path);
+      }
+    }
+    elseif (\Drupal::moduleHandler()->moduleExists('search')
+      && (
+        \Drupal::currentUser()->hasPermission('search content')
+        || \Drupal::currentUser()->hasPermission('search by page')
+      )) {
+
       // Get and use the default search engine for the site.
       $search_page_repository = \Drupal::service('search.search_page_repository');
       $default_search_page = $search_page_repository->getDefaultSearchPage();
@@ -216,38 +252,6 @@ class Search404Controller extends ControllerBase {
       ];
       $build['#attached']['library'][] = 'search/drupal.search.results';
     }
-    if (\Drupal::config('search404.settings')->get('search404_do_custom_search') &&
-    !\Drupal::config('search404.settings')->get('search404_skip_auto_search')) {
-      $custom_search_path = \Drupal::config('search404.settings')->get('search404_custom_search_path');
-
-      // Remove query parameters before checking whether the search path
-      // exists or the user has access rights.
-      $custom_search_path_no_query = preg_replace('/\?.*/', '', $custom_search_path);
-      $current_path = \Drupal::service('path.current')->getPath();
-      $current_path = preg_replace('/[!@#$^&*();\'"+_,]/', '', $current_path);
-
-      // All search keywords with space
-      // and slash are replacing with hyphen in url redirect.
-      $search_keys = '';
-      // If search with OR condition enabled.
-      if (\Drupal::config('search404.settings')->get('search404_use_or')) {
-        $search_details = $this->search404CustomRedirection(' OR ', $current_path, $keys);
-      }
-      else {
-        $search_details = $this->search404CustomRedirection(' ', $current_path, $keys);
-      }
-      $current_path = $search_details['path'];
-      $search_keys = $search_details['keys'];
-
-      // Redirect to the custom path.
-      if ($current_path == "/" . $keys || $current_path == "/" . $search_keys) {
-        $this->search404CustomErrorMessage($keys);
-        if ($search_keys != '') {
-          $custom_search_path = str_replace('@keys', $search_keys, $custom_search_path);
-        }
-        return $this->search404Goto("/" . $custom_search_path);
-      }
-    }

     if (empty($build)) {
       $build = ['#markup' => 'The page you requested does not exist.'];
