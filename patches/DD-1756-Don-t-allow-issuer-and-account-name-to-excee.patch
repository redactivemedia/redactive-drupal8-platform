From ddf263cf9fd13dab15578f4e495f5b16d5453093 Mon Sep 17 00:00:00 2001
From: Alex Harries <alex.harries@redactive.co.uk>
Date: Tue, 12 Mar 2024 13:30:35 +0000
Subject: [PATCH] DD-1756 Don't allow issuer and account name to exceed 63
 characters

Signed-off-by: Alex Harries <alex.harries@redactive.co.uk>
---
 src/Plugin/TfaSetup/TfaTotpSetup.php | 57 +++++++++++++++++++++-------
 1 file changed, 44 insertions(+), 13 deletions(-)

diff --git a/src/Plugin/TfaSetup/TfaTotpSetup.php b/src/Plugin/TfaSetup/TfaTotpSetup.php
index 90c27be..4ea0b7a 100644
--- a/src/Plugin/TfaSetup/TfaTotpSetup.php
+++ b/src/Plugin/TfaSetup/TfaTotpSetup.php
@@ -58,20 +58,20 @@ class TfaTotpSetup extends TfaTotpValidation implements TfaSetupInterface {
    *
    * @var string|array|string[]
    */
-  protected string $siteNameEncodedAndTrimmed;
+  protected string $siteNameTrimmed;
 
   /**
-   * The maximum length of account name and issuer (each).
+   * The maximum total length of account name and issuer.
    *
-   * I.e. if maximum length is 50, both account name and issuer can be 50
-   * characters in length, totalling 100.
+   * Security keys such as Yubikey have a 63 character limit on the account
+   * name and issuer combined.
    *
-   * This limit is needed because some 2FA gadgets such as Yubikeys have a limit
-   * on how long the account name and/or issuer can be.
+   * This value is used by trimAccountNameAndIssuer() to ensure we don't try
+   * to save a TOTP key which has an invalid length.
    *
    * @var int
    */
-  protected int $accountAndIssuerMaxLength = 50;
+  protected int $accountAndIssuerMaxTotalLength = 63;
 
   /**
    * The account name for the QR image.
@@ -79,7 +79,7 @@ class TfaTotpSetup extends TfaTotpValidation implements TfaSetupInterface {
    * We just use the user's username for this, trimmed to a maximum of 50
    * characters, url encoded (except for spaces).
    */
-  protected string $accountName;
+  protected string $accountNameTrimmed;
 
   /**
    * {@inheritdoc}
@@ -93,14 +93,15 @@ class TfaTotpSetup extends TfaTotpValidation implements TfaSetupInterface {
     // Get the site name.
     $this->siteName = \Drupal::config('system.site')->get('name');
 
-    // Set a trimmed, URL-encoded site name.
-    $this->siteNameEncodedAndTrimmed = substr($this->siteName, 0, $this->accountAndIssuerMaxLength);
-
     // Get and set the user's account name.
     /** @var \Drupal\user\Entity\User $account */
     $account = User::load($this->configuration['uid']);
 
-    $this->accountName = substr($account->getAccountName(), 0, $this->accountAndIssuerMaxLength);
+    $account_name_and_issuer_trimmed = $this->trimAccountNameAndIssuer($this->siteName, $account->getAccountName());
+
+    // Set a trimmed issuer and account name.
+    $this->siteNameTrimmed = $account_name_and_issuer_trimmed['issuer_name'];
+    $this->accountNameTrimmed = $account_name_and_issuer_trimmed['account_name'];
   }
 
   /**
@@ -198,7 +199,7 @@ class TfaTotpSetup extends TfaTotpValidation implements TfaSetupInterface {
    *   QR-code uri.
    */
   protected function getQrCodeUri() {
-    return (new QRCode())->render('otpauth://totp/' . $this->accountName . '?secret=' . $this->seed . '&issuer=' . $this->siteNameEncodedAndTrimmed);
+    return (new QRCode())->render('otpauth://totp/' . $this->accountNameTrimmed . '?secret=' . $this->seed . '&issuer=' . $this->siteNameTrimmed);
   }
 
   /**
@@ -275,4 +276,34 @@ class TfaTotpSetup extends TfaTotpValidation implements TfaSetupInterface {
     return ($this->pluginDefinition['setupMessages']) ?: '';
   }
 
+  /**
+   * Trim account and issuer name so they're not longer than
+   * accountAndIssuerMaxTotalLength.
+   *
+   * @param $issuer_name
+   * @param $account_name
+   *
+   * @return array
+   */
+  protected function trimAccountNameAndIssuer($issuer_name, $account_name) {
+    // If account name and issuer name are longer than
+    // $this->accountAndIssuerMaxTotalLength, trim the longer of the two by
+    // one character at a time until it's short enough.
+    while ((strlen($account_name) + strlen($issuer_name)) > $this->accountAndIssuerMaxTotalLength) {
+      // If account name is longer than issuer, trim account name.
+      if (strlen($account_name) > strlen($issuer_name)) {
+        $account_name = substr($account_name, 0, -1);
+      }
+      // Otherwise, if issuer name is the same or longer, trim that.
+      else {
+        $issuer_name = substr($issuer_name, 0, -1);
+      }
+    }
+
+    return [
+      'account_name' => $account_name,
+      'issuer_name' => $issuer_name,
+    ];
+  }
+
 }
-- 
2.23.0

