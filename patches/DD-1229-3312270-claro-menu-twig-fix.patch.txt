diff --git a/core/themes/claro/templates/navigation/menu-local-task.html.twig b/core/themes/claro/templates/navigation/menu-local-task.html.twig
index c2b19cd1f..7c8bb0de2 100644
--- a/core/themes/claro/templates/navigation/menu-local-task.html.twig
+++ b/core/themes/claro/templates/navigation/menu-local-task.html.twig
@@ -19,7 +19,7 @@
   {{ link }}
   {% if is_active and level == 'primary' %}
     <button class="reset-appearance tabs__trigger" aria-label="{{ 'Tabs display toggle'|t }}" data-drupal-nav-tabs-trigger>
-      {% include "@claro/../images/src/hamburger-menu.svg" %}
+      {% include "@claro/../images/src/hamburger-menu.svg" ignore missing %}
     </button>
   {% endif %}
 </li>
