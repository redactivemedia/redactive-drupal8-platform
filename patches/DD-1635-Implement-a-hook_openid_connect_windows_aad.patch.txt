From 21f8f452d9c4e827207c72c6dcc5e21db5bf6770 Mon Sep 17 00:00:00 2001
From: Alex Harries <alex.harries@redactive.co.uk>
Date: Tue, 26 Sep 2023 12:35:59 +0100
Subject: [PATCH] DD-1635 Implement a
 hook_openid_connect_windows_aad_retrieve_tokens_alter

Signed-off-by: Alex Harries <alex.harries@redactive.co.uk>
---
 openid_connect_windows_aad.module             | 25 +++++++++++++++++++
 src/Plugin/OpenIDConnectClient/WindowsAad.php |  8 ++++++
 2 files changed, 33 insertions(+)

diff --git a/openid_connect_windows_aad.module b/openid_connect_windows_aad.module
index 0d30b18..b1896a9 100644
--- a/openid_connect_windows_aad.module
+++ b/openid_connect_windows_aad.module
@@ -223,3 +223,28 @@ function openid_connect_windows_aad_openid_connect_userinfo_save(UserInterface $
   }
 
 }
+
+/**
+ * Implements hook_openid_connect_windows_aad_retrieve_tokens_alter().
+ *
+ * If your response is missing data such as the access_token field, you can
+ * set a fake one here to prevent errors.
+ *
+ * @param array|string|null $response_data
+ * @param \GuzzleHttp\Psr7\Response $response
+ * @param \GuzzleHttp\Client $client
+ * @param \Drupal\openid_connect_windows_aad\Plugin\OpenIDConnectClient\WindowsAad $openid_connect_client
+ *
+ * @return void
+ */
+function hook_openid_connect_windows_aad_retrieve_tokens_alter(&$response_data, \GuzzleHttp\Psr7\Response $response, \GuzzleHttp\Client $client, \Drupal\openid_connect_windows_aad\Plugin\OpenIDConnectClient\WindowsAad $openid_connect_client) {
+  // Cast $response_data to an array if it isn't already.
+  if (!is_array($response_data)) {
+    $response_data = [$response_data];
+  }
+
+  // Set a "fake" access token if none was returned in the request.
+  if (empty($response_data['access_token'])) {
+    $response_data['access_token'] = 'fake access token set in hook_openid_connect_windows_aad_retrieve_tokens_alter()';
+  }
+}
diff --git a/src/Plugin/OpenIDConnectClient/WindowsAad.php b/src/Plugin/OpenIDConnectClient/WindowsAad.php
index 6836a86..8bf552b 100644
--- a/src/Plugin/OpenIDConnectClient/WindowsAad.php
+++ b/src/Plugin/OpenIDConnectClient/WindowsAad.php
@@ -208,6 +208,14 @@ class WindowsAad extends OpenIDConnectClientBase {
       $response = $client->post($endpoints['token'], $request_options);
       $response_data = json_decode((string) $response->getBody(), TRUE);
 
+      // Implement a hook_openid_connect_windows_aad_retrieve_tokens_alter.
+      \Drupal::moduleHandler()->invokeAll('openid_connect_windows_aad_retrieve_tokens_alter', [
+        &$response_data,
+        $response,
+        $client,
+        $this,
+      ]);
+
       // Expected result.
       $tokens = [
         'id_token' => $response_data['id_token'],
-- 
2.23.0

