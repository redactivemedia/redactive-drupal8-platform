From 77cea9152f9c65e223febdf7de45792b434e52ab Mon Sep 17 00:00:00 2001
From: Alex Harries <alex.harries@redactive.co.uk>
Date: Mon, 4 Mar 2024 13:12:44 +0000
Subject: [PATCH] DD-1756 Improve messaging when 2FA setup is completed

Signed-off-by: Alex Harries <alex.harries@redactive.co.uk>
---
 src/Form/TfaSetupForm.php | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/src/Form/TfaSetupForm.php b/src/Form/TfaSetupForm.php
index d74d48f..29540c4 100644
--- a/src/Form/TfaSetupForm.php
+++ b/src/Form/TfaSetupForm.php
@@ -362,14 +362,14 @@ class TfaSetupForm extends FormBase {
         return;
       }
       // Else, setup complete and return to overview page.
-      $this->messenger()->addStatus($this->t('TFA setup complete.'));
+      $this->messenger()->addStatus($this->t('Two-factor Authentication setup complete. Important: please remember to generate and save your backup codes to prevent yourself from being locked out of your account.'));
       $form_state->setRedirect('tfa.overview', ['user' => $account->id()]);
 
       // Log and notify if this was full setup.
       if (!empty($storage['step_method'])) {
         $data = ['plugins' => $storage['step_method']];
         $this->tfaSaveTfaData($account->id(), $this->userData, $data);
-        $this->logger('tfa')->info('TFA enabled for user @name UID @uid', [
+        $this->logger('tfa')->info('Two-factor Authentication enabled for user @name UID @uid', [
           '@name' => $account->getAccountName(),
           '@uid' => $account->id(),
         ]);
-- 
2.23.0

