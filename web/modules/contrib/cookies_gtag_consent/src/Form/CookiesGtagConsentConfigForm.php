<?php

namespace Drupal\cookies_gtag_consent\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form to configure the COOKiES Gtag Consent module settings.
 */
class CookiesGtagConsentConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cookies_gtag_consent.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cookies_gtag_consent_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cookies_gtag_consent.config');

    $form['wait_for_update'] = [
      '#type' => 'number',
      '#title' => $this->t('Wait for update'),
      '#description' => $this->t('If your Consent Management Provider loads asynchronously, it might not always run before your Google Tags. To handle such situations, this setting adds the "wait_for_update" setting along with a millisecond value to control how long to wait before data is sent. Default is 500ms.'),
      '#default_value' => $config->get('wait_for_update'),
      '#min' => 0,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cookies_gtag_consent.config')
      ->set('wait_for_update', $form_state->getValue('wait_for_update'))
      ->save();
  }

}
