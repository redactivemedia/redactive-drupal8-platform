# COOKiES Google Tag Manager Consent (modified by Redactive)

---

Provides cookies integration with the new Google Tag Manager consent mode.

This module adds third party settings to the Cookies Service entity form to add
control over GTM consent settings.

**Important: this module has been modified by Redactive - be careful not to lose
any changes we have made if and when updating this module.**

_If this module gets a stable release, we may wish to either create patches to
preserve our modifications, or contribute back to the community._
