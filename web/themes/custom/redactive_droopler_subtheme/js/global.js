/**
 * @file
 * global.js
 *
 * JS tweaks for the Redactive Droopler Subtheme.
 *
 * Note that this file is minified by following the Gulp commands in the
 * README.md file.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.redactiveDrooplerSubtheme = {

    attach: function (context) {
      if (context !== document) {
        return;
      }

      // Indicate to redactive-customisations.js that the mobile breakpoint
      // is set at 992px.
      $('body').attr('data-mobile-menu-breakpoint', 992);

      // Sticky header.
      redactiveDrooplerSubthemeStickyHeader($('header#header'), $);
    }
  };

  /**
   * Sticky Header of awesomeness.
   *
   * When we scroll down the page, add a class which smoothly transitions into
   * a more-compact header with no three-column CTA block.
   *
   * @param $headerElement
   * @param $
   */
  function redactiveDrooplerSubthemeStickyHeader($headerElement, $) {
    // How far we should be from the top of the page before we shrink the
    // header.
    var pixelsToScrollFromTopBeforeAddingClass = 100;

    // How far to scroll down without scrolling up before we decrease the
    // header size.
    var pixelsToScrollDownBeforeAddingClass = 20;

    // How far to scroll up without scrolling down before we increase the
    // header size.
    var pixelsToScrollUpBeforeRemovingClass = 40;

    // The class to add to shrink the header, on scroll down.
    var classToAddOnScrollDown = 'header-scrolled-from-top';

    var currentScrollTop = 0;
    var lastScrollTop = 0;

    var windowHeight = $(window).height();
    var pageHeight = $('html').height();
    var pseudoHeaderClass = 'header-pseudo-header';
    var pseudoHeaderSelector = '.' + pseudoHeaderClass;

    var pixelsScrolledUpThisTime = 0;
    var pixelsScrolledDownThisTime = 0;

    // Ignore the first few scroll events on the page - this helps prevent
    // the UI from jumping around as the page loads.
    var pageLoadIgnoreFirstXScrollEvents = 10;
    var scrollEventsCounter = 0;

    if ($headerElement.length) {
      // Create a <div> which will sit under the header.
      $headerElement.parent().prepend('<div class="' + pseudoHeaderClass + '"></div>');

      // Add the fixed class to the header, and set its position to match the
      // top of the .page-wrapper.
      var pageWrapperOffsetTop = $('.page-wrapper').offset().top;
      $headerElement.addClass('header-fixed').css('top', pageWrapperOffsetTop);
      // console.log('$headerElement top position set to ' + pageWrapperOffsetTop);

      // Also set the top position of the mobile menu.
      $('header.header > nav.navbar-wrapper nav.main-navbar').css('top', $headerElement.height());
      // console.log('nav.main-navbar top position set to ' + $headerElement.height());

      // Set the pseudo-header height.
      $(pseudoHeaderSelector).height($headerElement.height());

      // If the page is already scrolled down, add the scrolled class.
      if (($(window).scrollTop() > pixelsToScrollFromTopBeforeAddingClass) && !($headerElement.hasClass(classToAddOnScrollDown))) {
        $headerElement.addClass(classToAddOnScrollDown);
      }

      // Detect whether we're scrolling down or up.
      $(window).scroll(function() {
        // console.log('Scroll detected...');

        if (scrollEventsCounter < pageLoadIgnoreFirstXScrollEvents) {
          // console.log('Ignoring scroll event ' + scrollEventsCounter + ' out of first ' + pageLoadIgnoreFirstXScrollEvents + ' scroll events');

          // Increment the counter.
          scrollEventsCounter++;

          return;
        }

        // Get the current scroll top position.
        currentScrollTop = $(window).scrollTop();

        // console.log('currentScrollTop=' + currentScrollTop + ' lastScrollTop=' + lastScrollTop);

        // Don't do anything if the page is shorter than the header height plus the content.
        if (pageHeight > (windowHeight + $headerElement.height() + pixelsToScrollFromTopBeforeAddingClass)) {
          // console.log('Page is long enough to animate (shrink) the header');

          // If we're scrolling down, reset pixelsToScrollUpBeforeRemovingClass to
          // zero.
          if (currentScrollTop > lastScrollTop) {
            pixelsScrolledUpThisTime = 0;
          }
          // Or if we're scrolling up, reset the downward counter.
          else if (currentScrollTop < lastScrollTop) {
            pixelsScrolledDownThisTime = 0;
          }

          // If we're scrolling down, and the page is scrolled more than
          // pixelsToScrollFromTopBeforeAddingClass pixels, and the scroll class
          // isn't already present, apply the class.
          if (currentScrollTop > lastScrollTop) {
            if (!($headerElement.hasClass(classToAddOnScrollDown))) {
              // If the user has scrolled down more than
              // pixelsScrolledDownThisTime, and they are further than
              // pixelsToScrollFromTopBeforeAddingClass from the top of the page,
              // add the class.
              // console.log('currentScrollTop=' + currentScrollTop + ' lastScrollTop=' + lastScrollTop);
              // console.log(pixelsScrolledDownThisTime, 'pixelsScrolledDownThisTime1');
              // console.log(pixelsToScrollDownBeforeAddingClass, 'pixelsToScrollDownBeforeAddingClass');

              if ((pixelsScrolledDownThisTime > pixelsToScrollDownBeforeAddingClass)
                && (currentScrollTop > pixelsToScrollFromTopBeforeAddingClass)) {

                pixelsScrolledDownThisTime = 0;
                $headerElement.addClass(classToAddOnScrollDown);
              }
              else {
                // console.log('Scrolled down but not enough');
                pixelsScrolledDownThisTime = pixelsScrolledDownThisTime + (currentScrollTop - lastScrollTop);
                // console.log(currentScrollTop, 'currentScrollTop');
                // console.log(pixelsScrolledDownThisTime, 'pixelsScrolledDownThisTime2');
                // console.log(pixelsToScrollFromTopBeforeAddingClass, 'pixelsToScrollFromTopBeforeAddingClass');
              }
            }
          }
          else if (currentScrollTop < lastScrollTop) {
            if ($headerElement.hasClass(classToAddOnScrollDown)) {
              // Otherwise, if we're scrolling up (currentScrollTop is less than
              // (lastScrollTop minus pixelsToScrollUpBeforeRemovingClass)) or
              // we're at the top of the page, track how far up we've scrolled up
              // and when we get to pixelsToScrollUpBeforeRemovingClass without
              // scrolling downwards at all, remove the scroll class from the header
              // and reset pixelsToScrollUpBeforeRemovingClass.
              // console.log('currentScrollTop=' + currentScrollTop + ' lastScrollTop=' + lastScrollTop);
              // console.log(pixelsScrolledUpThisTime, 'pixelsScrolledUpThisTime1');
              // console.log(pixelsToScrollUpBeforeRemovingClass, 'pixelsToScrollUpBeforeRemovingClass');

              if ((pixelsScrolledUpThisTime > pixelsToScrollUpBeforeRemovingClass)
                || (currentScrollTop === 0)) {
                $headerElement.removeClass(classToAddOnScrollDown);
                pixelsScrolledUpThisTime = 0;
              }
              else {
                // console.log('Scrolled up but not enough');
                pixelsScrolledUpThisTime = pixelsScrolledUpThisTime + (lastScrollTop - currentScrollTop);
                // console.log(pixelsScrolledUpThisTime, 'pixelsScrolledUpThisTime2');
              }
            }
          }

          // Only set the pseudo-header's height if the actual header is taller.
          if ($headerElement.height() > $(pseudoHeaderSelector).height()) {
            $(pseudoHeaderSelector).animate({height: $headerElement.height()}, 100);
          }

          // Update last scroll top.
          lastScrollTop = currentScrollTop;
        }
      });

      // Recalculate header height on resize.
      $(window).resize(function() {
        // Change the height of the pseudo-header element to match the new
        // height of the header element, if it's changed.
        $(pseudoHeaderSelector).height($headerElement.height());
      });
    }
  }

  // Carousels.
  // @todo: these should not be using document ready.
  $(document).ready(function(){
    $('.layout-carousel .view-content').slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      dots: true,
      focusOnSelect: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
          }
        }
      ]
    });
  });

})(jQuery, Drupal);
