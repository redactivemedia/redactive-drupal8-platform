# Redactive Droopler Subtheme

This is a fork of the Bootstrap 4 Droopler Subtheme.

To recompile this theme's CSS, you must first make sure all node modules have been installed in the parent theme (redactive_droopler_theme):

```
cd path/to/redactive_droopler_theme
npm install
gulp compile
```

**(Note: we periodically encounter a problem where `npm install` freezes for a very long time. If this happens to you, you may need to copy the package.json and gulpfile.js from the redactive-drupal8-platform repo as this has probably already been fixed there.)**

Then do:

```
cd path/to/redactive_droopler_subtheme
npm install
```

... and finally:

```
gulp watch
```

... or:

```
gulp compile
```
