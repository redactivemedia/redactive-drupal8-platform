<?php

/**
 * @file
 *
 * Add a chunk of HTML for the Fast404 message to $settings['fast404_html'].
 *
 * Note that this HTML contains Twig-like placeholders for local site HTML
 * injections at the top and bottom of both the head and body tags.
 */

$settings['fast404_html'] = <<<MONKEY

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>

    {{ head__top_of_head }}

    <title>404 Not Found</title>
    <style type="text/css">
      body {
        font-family: Arial, sans-serif;
        font-size: 1.2em;
        padding: 0 1em 1em 1em;
        text-align: center;
      }

      h1 {
        font-weight: normal;
      }

      body,
      .searchform,
      input {
        color: #555;
      }

      .searchform {
        margin: 1em 0;
        padding: 1em;
        background-color: #efefef;
      }

      .searchform input {
        font-size: 1em;
        max-width: 100%;
      }

      .searchform label,
      .searchform input {
        display: inline-block;
        margin: 0.2em;
      }
    </style>

    {{ head__bottom_of_head }}

  </head>
  <body>

    {{ body__top_of_body }}

    <h1>Not Found</h1>
    <p>Sorry; the requested page <em>@path</em> doesn't exist on this site.</p>

    <form method="get" action="/search" class="searchform">
      <label for="s">Search the site for:</label>
      <input name="s" id="s" type="text" value="" size="60" maxlength="128"/>
      <input type="submit" id="submit" value="Search"/>
    </form>
    <p>Or, <a href="/?utm_source=fast404-error">return to the homepage</a>.</p>

    <script>
      var path = window.location.pathname;

      // URL decode the path.
      path = decodeURI(path);

      // Remove any non-alphanumerics, then remove numbers, replace duplicate
      // spaces with one space, and trim whitespace.
      path = path.replace(/[\W_]+/g, " ").replace(/[0-9]/g, " ").replace(/  +/g, " ").trim();

      // Set any resulting words in the search field.
      document.getElementById("s").value = path;
    </script>

    {{ body__bottom_of_body }}

  </body>
</html>

MONKEY;
