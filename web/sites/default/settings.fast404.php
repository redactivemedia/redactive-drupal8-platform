<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Fast 404 settings.
 *
 * Fast 404 will do three separate types of 404 checking.
 *
 * The first is to check for URLs which appear to be files or images. If Drupal
 * is handling these items, then they were not found in the file system and are
 * a 404.
 *
 * The second is to check whether or not the URL exists in Drupal by checking
 * with the menu router, aliases and redirects. If the page does not exist, we
 * will serve a Fast 404 error and exit.
 *
 * The third is to listen to the KernelRequest event. If Drupal returns a
 * NotFoundHttpException exception, Fast 404 can intervene and serve a Fast 404
 * error and exit.
 */

/**
 * Disallowed extensions. Any extension in here will not be served by Drupal
 * and will get a Fast 404. This will not affect actual files on the filesystem
 * as requests hit them before defaulting to a Drupal request.
 * Default extension list, this is considered safe and matches the list provided
 * by Drupal's $config['system.performance']['fast_404']['paths'].
 *
 * Default value for this setting is shown below.
 */
$settings['fast404_exts'] = '/^(?!robots).*\.(txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';

/**
 * If you use a private file system use the settings variable below and change
 * the 'sites/default/private' to your actual private files path.
 */
# $settings['fast404_exts'] = '/^(?!robots)^(?!sites/default/private).*\.(txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';

/**
 * Allow anonymous users to hit URLs containing 'imagecache' even if the file
 * does not exist. TRUE is default behavior. If you know all imagecache
 * variations are already made set this to FALSE.
 *
 * Default value for this setting is TRUE.
 */
# $settings['fast404_allow_anon_imagecache'] = FALSE;

/**
 * BE CAREFUL with this setting as some modules use their own php files and you
 * need to be certain they do not bootstrap Drupal. If they do, you will need to
 * whitelist them too.
 *
 * Default value for this setting is FALSE.
 */
# $settings['fast404_url_whitelisting'] = TRUE;

/**
 * Array of whitelisted files/urls. Used if whitelisting is set to TRUE.
 *
 * Default value for this setting is an empty array.
 */
# $settings['fast404_whitelist'] = ['index.php', 'rss.xml', 'install.php', 'cron.php', 'update.php', 'xmlrpc.php'];

/**
 * Array of whitelisted URL fragment strings that conflict with fast404.
 *
 * Default value for this setting is FALSE.
 */
$settings['fast404_string_whitelisting'] = [
  // system/files added per https://drupal.stackexchange.com/a/260108
  'system/files',

  // Some other possible examples below.
//  'cdn/farfuture',
//  '/advagg_',
//  'structure/menu/item/',
//  '/flag',
//  'group/',
//  '/taxonomy/term/',
//  'media/',
];

/**
 * When Fast 404 checks for missing file assets, it will return a response
 * containing the message set in the fast404_html settings. You can override
 * this behavior with this setting and return the HTML from the file set in the
 * fast404_HTML_error_page setting.
 *
 * Default value for this setting is FALSE.
 */
$settings['fast404_HTML_error_all_paths'] = TRUE;

/**
 * Subscribe to NotFoundHttpException event.
 *
 * The Fast 404 Event subscriber can listen to the NotFoundHttpException event
 * to completely replace the Drupal 404 page.
 *
 * By default, Fast 404 only listens to KernelRequest event. If a user hits a
 * valid path, but another module intervenes and returns a NotFoundHttpException
 * exception (eg. m4032404 module), the native Drupal 404 page is returned
 * instead of the Fast 404 page.
 *
 * Default value for this setting is FALSE.
 */
$settings['fast404_not_found_exception'] = TRUE;

/**
 * Path checking. USE AT YOUR OWN RISK.
 *
 * Path checking at this phase is more dangerous, but faster. Normally
 * Fast 404 will check paths during Drupal bootstrap via an early Event.
 * While this setting finds 404s faster, it adds a bit more load time to
 * regular pages, so only use if you are spending too much CPU/Memory/DB on
 * 404s and the trade-off is worth it.
 *
 * This setting will deliver 404s with less than 2MB of RAM.
 *
 * Default value for this setting is FALSE.
 */
$settings['fast404_path_check'] = TRUE;

/**
 * If you would prefer a stronger version of NO then return a 410 instead of a
 * 404. This informs clients that not only is the resource currently not
 * present but that it is not coming back and kindly do not ask again for it.
 * Reference: http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 *
 * Default value for this setting is FALSE.
 */
$settings['fast404_return_gone'] = TRUE;

/**
 * If you use FastCGI, uncomment this line to send the type of header it needs.
 * Reference: http://php.net/manual/en/function.header.php
 *
 * Default value for this setting is 'mod_php'.
 */
# $settings['fast404_HTTP_status_method'] = 'FastCGI';

/**
 * Allow sites to add to the Fast404 custom error page.
 *
 * This is useful for example if we want to include a Google Tag Manager
 * script, or similar.
 *
 * This file should set any custom text it wishes to in an array key in a
 * variable called $fast404_html_injections - this array should have the
 * following hopefully self-explanatory keys:
 *
 * - head__top_of_head
 * - head__bottom_of_head
 * - body__top_of_body
 * - body__bottom_of_body
 */
$fast404_html_injections = [
  'head__top_of_head' => '',
  'head__bottom_of_head' => '',
  'body__top_of_body' => '',
  'body__bottom_of_body' => '',
];

$include = __DIR__ . "/settings.fast404.html_injections.php";
if (file_exists($include)) {
  include $include;
}

/**
 * Fast 404 error message.
 *
 * The default value for this setting is set, then we try to overwrite it if
 * we have a custom HTML file.
 *
 * Note that this HTML contains Twig-like placeholders for local site HTML
 * injections at the top and bottom of both the head and body tags.
 */

$settings['fast404_html'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>

    {{ head__top_of_head }}

    <title>404 Not Found</title>
    <style type="text/css">
      body {
        font-family: Arial, sans-serif;
        color: #444;
      }
    </style>

    {{ head__bottom_of_head }}

  </head>
  <body>

    {{ body__top_of_body }}

    <h1>Not Found</h1>
    <p>Sorry; the requested page "@path" doesn\'t exist on this site.</p>
    <p>Please <a href="/?utm_source=fast404-error">return to the homepage and continue your search from there</a>.</p>

    {{ body__bottom_of_body }}

  </body>
</html>';

$include = __DIR__ . "/settings.fast404.html.php";
if (file_exists($include)) {
  include $include;
}

/**
 * Replace the Fast404 placeholders with the contents of the
 * $fast404_html_injections array.
 */
foreach ($fast404_html_injections as $fast404_html_injection_label => $fast404_html_injection) {
  $settings['fast404_html'] = str_replace('{{ ' . $fast404_html_injection_label . ' }}', $fast404_html_injection, $settings['fast404_html']);
}

/**
 * Load the fast404.inc file.
 *
 * This is needed if you wish to do extension checking in settings.php.
 *
 * WARNING: This is not fully implemented and not ready to use.
 * @see: https://www.drupal.org/project/fast_404/issues/2961512
 *
 */
if (!empty($app_root)
  && file_exists($app_root . '/modules/contrib/fast_404/fast404.inc')) {
  // Hotfix: remove the following include. The warning above is actually
  // correct; we have app logs for IOSH with thousands of the following errors:

  /*
    [21-Sep-2020 22:31:36 UTC] Drupal\Core\DependencyInjection\ContainerNotInitializedException: \Drupal::$container is not initialized yet. \Drupal::setContainer() must be called with a real container. in /app/web/core/lib/Drupal.php on line 130 #0 /app/web/core/lib/Drupal.php(158): Drupal::getContainer()
    #1 /app/web/core/lib/Drupal/Core/StringTranslation/StringTranslationTrait.php(104): Drupal::service('string_translat...')
    #2 /app/web/core/lib/Drupal/Core/StringTranslation/StringTranslationTrait.php(71): Drupal\fast404\Fast404->getStringTranslation()
    #3 /app/web/modules/contrib/fast_404/src/Fast404.php(218): Drupal\fast404\Fast404->t('The requested U...', Array)
    #4 /app/web/modules/contrib/fast_404/fast404.inc(41): Drupal\fast404\Fast404->response()
    #5 /app/web/sites/default/settings.fast404.php(179): fast404_preboot(Array)
    #6 /app/web/sites/default/settings.php(81): include('/app/web/sites/...')
    #7 /app/web/core/lib/Drupal/Core/Site/Settings.php(125): require('/app/web/sites/...')
    #8 /app/web/core/lib/Drupal/Core/DrupalKernel.php(1073): Drupal\Core\Site\Settings::initialize('/app/web', 'sites/default', Object(Composer\Autoload\ClassLoader))
    #9 /app/web/core/lib/Drupal/Core/DrupalKernel.php(698): Drupal\Core\DrupalKernel->initializeSettings(Object(Symfony\Component\HttpFoundation\Request))
    #10 /app/web/index.php(19): Drupal\Core\DrupalKernel->handle(Object(Symfony\Component\HttpFoundation\Request))
    #11 {main}
   */
  //include_once $app_root . '/modules/contrib/fast_404/fast404.inc';
  //fast404_preboot($settings);
}

/**
 * Check the Redirects table.
 *
 * (Shurely this is just basic common sense...?!)
 *
 * @see https://www.drupal.org/project/fast_404/issues/2838359#comment-14574011
 */
$settings['fast404_respect_redirect'] = TRUE;
