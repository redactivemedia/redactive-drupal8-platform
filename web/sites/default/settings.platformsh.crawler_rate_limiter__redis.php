<?php

/**
 * @file
 *
 * settings.platformsh.crawler_rate_limiter__redis.php
 *
 * Crawler Rate Limit settings for sites with Redis (i.e. platform.sh).
 *
 * This file should only be included when we know that Redis is available.
 *
 * Sample code to check:
 *
 * if ($platformsh->hasRelationship('rediscache')
 *   && !InstallerKernel::installationAttempted()
 *   && extension_loaded('redis')
 *   && class_exists('Drupal\redis\ClientFactory')) {
 */

// Below configuration uses Redis backend and will allow crawlers to perform
// 100 requests within 600 seconds. It will also rate limit regular traffic
// allowing 300 requests within 600 seconds.

// Enable or disable rate limiting. Required.
$settings['crawler_rate_limit.settings']['enabled'] = TRUE;

// Supported backends: redis, memcached, apcu. Required.
$settings['crawler_rate_limit.settings']['backend'] = 'redis';

// Limit for crawlers and bots traffic. Required.
$settings['crawler_rate_limit.settings']['bot_traffic'] = [
  // Time interval in seconds. Must be whole number greater than zero.
  'interval' => 600,

  // Number of requests allowed in the given time interval. Must be whole
  // number greater than zero.
  'requests' => 50,
];

// Limit for regular website traffic. Optional. If this setting is not
// provided, regular traffic won't be rate limited. Set both values to 0 in
// order to disable limiting of regular traffic.
$settings['crawler_rate_limit.settings']['regular_traffic'] = [
  'interval' => 600,
  'requests' => 300,
];

// Allow specified IP addresses to bypass rate limiting - useful if your
// website is maintained by a number of users all accessing the site from
// the same location, using the same browsers, and at the same time.

// Allows should be specified as IP addresses or ranges in a
// comma-separated list - for example:
// 1.2.3.4,1.2.3.0/8,5.4.3.2
// e.g. $settings['crawler_rate_limit.settings']['ip_address_allows'] = '1.2.3.4,1.2.3.0/8,5.4.3.2';

// Redactive's Fora office: 167.98.74.54
// Alex's VPN: 209.97.176.238
$settings['crawler_rate_limit.settings']['ip_address_allows'] = '167.98.74.54,209.97.176.238';
