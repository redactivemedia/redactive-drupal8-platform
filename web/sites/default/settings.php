<?php

/**
 * (Try to) limit PHP execution time to five minutes to prevent runaway
 * Drush crons from killing platform.sh sites.
 */
ini_set('max_execution_time', 300);

// Default Drupal settings.
//
// These are already explained with detailed comments in Drupal's
// default.settings.php file.
//
// See https://api.drupal.org/api/drupal/sites!default!default.settings.php/8
$databases = $databases ?? [];
$settings = $settings ?? [];

$settings['update_free_access'] = FALSE;

$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];

/**
 * Load services definition file.
 *
 * i.e. This should be located at sites/default/services.yml.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Place the config directory outside of the Drupal root.
 */
$settings['config_sync_directory'] = '../config/sync';

// Automatic Platform.sh settings.
$platformsh_settings = __DIR__ . "/settings.platformsh.php";
if (file_exists($platformsh_settings)) {
  include $platformsh_settings;
}

/**
 * If there is a local settings file, then include it.
 *
 * We no longer use this file in local development; instead, we use
 * settings.lando.php.
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}

/**
 * If there is a Lando settings file, then include it. This file will be
 * ignored unless we're running in a Lando environment.
 *
 * This file replaces settings.local.php
 */
$lando_settings = __DIR__ . "/settings.lando.php";
if (file_exists($lando_settings)) {
  include $lando_settings;
}

/**
 * If there is a migration settings file, include it.
 */
$migrate_database_settings = __DIR__ . "/settings.migrate-database.php";
if (file_exists($migrate_database_settings)) {
  include $migrate_database_settings;
}

/**
 * Set the default location for the 'private' directory if not yet set.
 *
 * We place this after settings.platformsh.php is included, because that
 * file only overrides file_private_path if it isn't already set.
 */
if (empty($settings['file_private_path'])) {
  // Attempt to create the private directory.
  $private_dir = __DIR__ . '/files/private';

  // If the directory doesn't yet exist, attempt to create it.
  if (!is_dir($private_dir)) {
    @mkdir($private_dir, 0700, TRUE);
  }

  // If the directory (now) exists, set file_private_path.
  if (is_dir($private_dir)) {
    $settings['file_private_path'] = $private_dir;
  }
}

/**
 * Set the default location for the 'temporary' directory if not yet set.
 *
 * We place this after settings.platformsh.php is included, because that
 * file only overrides file_temp_path if it isn't already set.
 */
if (empty($settings['file_temp_path'])) {
  // Attempt to create the private directory.
  $private_tmp_dir = __DIR__ . '/files/private/tmp';

  // If the directory doesn't yet exist, attempt to create it.
  if (!is_dir($private_tmp_dir)) {
    @mkdir($private_tmp_dir, 0700, TRUE);
  }

  // If the directory (now) exists, set file_private_path.
  if (is_dir($private_tmp_dir)) {
    $settings['file_temp_path'] = $private_tmp_dir;
  }
}

/**
 * Always set the install profile name.
 *
 * We always install the 'redactived8platformprofile' profile to stop the
 * installer from modifying settings.php.
 *
 * Removed in DD-791: this line is now deprecated.
 * @see https://www.drupal.org/node/2538996
 */
//$settings['install_profile'] = 'redactived8platformprofile';

/**
 * Load fast404 configuration, if available.
 *
 * Use settings.fast404.php to provide settings for Fast 404 module.
 *
 * Keep this code block at the end of this file to take full effect.
 */
$fast404_settings = __DIR__ . "/settings.fast404.php";
if (file_exists($fast404_settings)) {
  include $fast404_settings;
}
