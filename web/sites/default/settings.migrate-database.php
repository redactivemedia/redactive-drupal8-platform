<?php

/**
 * @file
 * Migration database settings.
 *
 * This file contains the database connection information for this particular
 * instance of a Drupal website which will be using the Migrate module to
 * import content from an external database.
 *
 * @see The Journal Migration module
 * (https://bitbucket.org/redactivemedia/redactived8-migration-journal) for more
 * information.
 */

/**
 * Migration database for <some website>.
 */

// See .lando.yml in this repo's root directory for the database name,
// username and password.

// Uncomment the following lines and change YOURSITE to match the site machine
// name in .lando.yml to allow Drupal to connect to your database.

// @see README.md in the Redactive Migrate module directory for more info.

// At the time of writing, the most-up-to-date version of these instructions
// is contained in https://bitbucket.org/redactivemedia/redactived8-migration-journal/src/master/README.md

// $databases['migrate']['default'] = [
//   // Make sure the database name is correct: this might be e.g. 'scraper_YOURSITE'
//   // or simply 'database'.
//   'database' => 'database',
//   'username' => 'root',
//   'password' => '',
//   'host' => 'YOURSITEmigration',
//   'port' => '3306',
//   'namespace' => 'Drupal\Core\Database\Driver\mysql',
//   'driver' => 'mysql',
// ];
