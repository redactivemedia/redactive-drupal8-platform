<?php

/**
 * @file
 *
 * This file can be used to add custom HTML injections on the Fast404 error
 * page on a per-site basis.
 *
 * To use this file, rename it to "settings.fast404.html_injections.php" and
 * edit one or more of the commented-out variables below.
 */

/*
 * HTML to be inserted at the top of the head tag.
 */
//$fast404_html_injections['head__top_of_head'] = '<!-- Top of head. -->';

/*
 * HTML to be inserted at the bottom of the head tag.
 */
//$fast404_html_injections['head__bottom_of_head'] = '<!-- Bottom of head. -->';

/*
 * HTML to be inserted at the top of the body tag.
 */
//$fast404_html_injections['body__top_of_body'] = '<!-- Top of body. -->';

/*
 * HTML to be inserted at the bottom of the body tag.
 */
//$fast404_html_injections['body__bottom_of_body'] = '<!-- Bottom of body. -->';

