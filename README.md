# Redactive Drupal Platform

This template builds Drupal using Redactive Media's Drupal Platform profile.

Please note: this build requires the https://bitbucket.org/redactivemedia/redactived8platformprofile profile; unfortunately, this is only available internally at Redactive as it contains our special secret Drupal sauce :o)

## One-Click Installer

Click the below button to initialise a new project on platform.sh with this profile - please set "Production environment" to `master`:

<p style="text-align: center;">
    <a href="https://console.platform.sh/projects/create-project?template=https://bitbucket.org/redactivemedia/redactive-drupal8-platform/raw/223c221e632bb1e9f275b7c2d10778009d37228f/platform.template.yaml">
        <img src="https://platform.sh/images/deploy/lg-blue.svg" alt="Deploy on Platform.sh" width="170px" />
    </a>
</p>

<hr />

### 0: Add your project's SSH deploy key to the Redactive Profile repo

After the initial build (which will fail), you will need to add your new platform.sh Deploy Key to the [Redactive Drupal Profile](https://bitbucket.org/redactivemedia/redactived8platformprofile).

You can find your project's deploy key by going to your platform.sh project's settings page > "project settings" > and click "Deploy Key" in the left menu. Your deploy key should begin `ssh-rsa .....`

You can add your deploy key to the [Redactive Drupal Profile here in Project Settings > Access Keys](https://bitbucket.org/redactivemedia/redactived8platformprofile/admin/access-keys/).

N.b. You will need to generate the `composer.lock` file on your machine by following the steps below; this can't be done on platform.sh due to memory constraints.

### 1: Download the platform.sh project's code

Follow the quick-start wizard in your platform.sh project to clone the project's code to your local machine and connect it to platform.sh - e.g. with `platform get PROJECTID`

The `cd` into your project's directory.

### 2: Set your Lando app name

Edit `.lando.yml` and change the application `name:` key to the same machine name as your platform.sh project - e.g. `redactive-drupal-mywebsite`

Run `git add .lando.yml && git commit -m "Set the Lando app name"`

### 3: Generate the Composer lockfile

In Terminal, run `COMPOSER_MEMORY_LIMIT=-1 composer install -v` to generate your lockfile, then commit it with:

`git add composer.lock && git commit -m "Create the lockfile"`

Then push your lockfile:

`git push platform master`

The output from your Git command should show that platform.sh is building your project.

Once built, you may be able to get Drupal to install itself on platform.sh by visiting your application's Master URL - e.g.:

https://www.master-7rqtwti-6wm36bezb3eim.eu-5.platformsh.site

However, the installer may time out, in which case follow the steps below to install Drupal on your local machine and then push the database up to platform.sh.

### 4: Initialise Lando

Run `lando start -vvv && lando info` to build your Lando container

### 5: Install Drupal

Lastly, to install Drupal on your Lando build, run the following command, choosing a new password and replacing
   {{THESE PARTS}} accordingly:

```
lando drush site-install redactived8platformprofile -y \
--account-name="{{ADMIN ACCOUNT NAME}}" \
--account-mail="digital+{{WEBSITE MACHINE NAME}}@redactive.co.uk" \
--account-pass="CorrectHorseBatteryStaple123" \
--site-mail="noreply@{{DOMAIN NAME}}" \
--site-name="{{WEBSITE FRIENDLY NAME}}"
```

The install process can take up to 30 minutes (possibly even longer on machines with fewer resources or where Docker is allocated less CPU).

You will need to run Drupal's updates again at least once after install:

`lando drush updb -y`

### 6: Log in to your local build

Once installation is complete, you can run `lando info && lando drush uli` to get your site's local domain name and a one-time login link.

### 7: Send the local database to platform.sh

Lastly, you can export the database from Lando and import it into your platform.sh site with the following commands:

`lando db-export my-new-site.mysql`

`platform sql -e master < my-new-site.mysql`

## More information

For more-detailed setup information, please see ["Local environment setup instructions" in the Redactive Developer Documentation](https://docs.google.com/document/d/1ROmOkw7a0lAbOKxfTtPMmaBciEnGuryWnzcGZmLvFbo/edit#heading=h.cwa3ura793hh).
